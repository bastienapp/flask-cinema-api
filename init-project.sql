CREATE DATABASE flask_cinema;
USE flask_cinema;

CREATE USER 'Flagpole2271'@'localhost' IDENTIFIED BY 'AfXiu5qap6cwHrMLih!HDmd$Bwr9SdR%';

GRANT ALL PRIVILEGES ON flask_cinema.* TO 'Flagpole2271'@'localhost';

CREATE TABLE movie (
	movie_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	title VARCHAR(255) NOT NULL,
	duration INT NOT NULL DEFAULT 0,
	release_year YEAR,
	rate FLOAT NOT NULL DEFAULT 0
);

INSERT INTO movie (title, duration, release_year, rate)
	VALUES ("Alien", 138, 1984, 8.6);

INSERT INTO movie (title, duration, release_year, rate)
	VALUES ("Interstellar", 179, 2014, 9.0);

INSERT INTO movie (title, duration, release_year, rate)
	VALUES ("Shaun of the Dead", 99, 2004, 10.0);
