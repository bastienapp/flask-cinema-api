from flask import Blueprint, request, jsonify

from app import mysql

director_controller = Blueprint('directors', __name__)

@director_controller.route('', methods=['GET'])
def get_list_directors():
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM director')
    director_list = cursor.fetchall()

    return jsonify(director_list), 200

@director_controller.route('', methods=['POST'])
def post_director():
    director_data = request.get_json()

    if 'director_fullname' not in director_data:
        return jsonify({
            'message': "director_fullname est manquant"
        }), 400

    fullname = director_data['director_fullname']
    # si fullname est vide, renvoyer une erreur
    if not fullname:
        return jsonify({
            'message': "director_fullname est vide"
        }), 400

    cursor = mysql.connection.cursor()
    cursor.execute("INSERT INTO director (director_fullname) VALUES (%s)",
                   (fullname,))
    mysql.connection.commit()

    director_id = cursor.lastrowid

    return jsonify({
        'director_id': director_id,
        'director_fullname': fullname
    }), 201

# TODO : create CRUD
