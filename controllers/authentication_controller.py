from flask import Blueprint, request, jsonify
from app import mysql
from app import bcrypt

auth_controller = Blueprint('authentication', __name__)

@auth_controller.route('/register', methods=['POST'])
def register():
    # récupérer les données du JSON
    user = request.json
    # TODO vérifier que les email et password sont bien renseignés
    # TODO vérifier qu'un compte n'existe pas avec cet email

    # mot de passe en clair : tacostacos
    # salage (salting) : tacos$poulpe$tacos tacos$castor$tacos
    # chiffrement (encryptage)
    # hacher le mot de passe
    password_hashed = bcrypt.generate_password_hash(
        user.get('password')).decode()

    # requête SQL d'insertion du compte utilisateur
    cursor = mysql.connection.cursor()
    cursor.execute(
        'INSERT INTO user (email, password) VALUES (%s, %s)',
        (user.get('email'), password_hashed,)
    )
    mysql.connection.commit()

    user_id = cursor.lastrowid

    return jsonify({
        'user_id': user_id,
        'email': user.get('email')
    }), 201

@auth_controller.route('/login', methods=['POST'])
def login():
    # récupération du corps de la requête
    user_data = request.json

    # email : vérifier si l'user existe avec l'email
    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT * FROM user WHERE email = %s',
        (user_data.get('email'),)
    )
    existing_user = cursor.fetchone()  # user_id, email, password
    # s'il n'existe pas
    if (existing_user is None):
        return jsonify({'error': 'Invalid email'}), 401

    # s'il existe : comparer le mot de passe haché en base avec le mot de passe en clair
    hashed_password = existing_user.get('password')
    clear_password = user_data.get('password')
    if (bcrypt.check_password_hash(hashed_password, clear_password)):
        return jsonify({
            'user_id': existing_user.get('user_id'),
            'email': existing_user.get('email')
        }), 200
    else:
        return jsonify({'error': 'Invalid password'}), 401
