from flask import Blueprint, request, jsonify
from app import mysql

movie_controller = Blueprint('movies', __name__)

@movie_controller.route('', methods=['GET'])
def get_list_movies():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les livres de la table book
    cursor.execute('SELECT * FROM movie')
    # Récupère tous les résultats de la requête
    movie_list = cursor.fetchall()

    # Convertit la liste de dictionnaires en JSON et la renvoie
    return jsonify(movie_list)

# ce endpoint sert à quoi ? l'identifiant il sert à quoi ?
@movie_controller.route('/<int:movie_id>', methods=['GET'])
def get_movie_by_id(movie_id):
    cursor = mysql.connection.cursor()
    # SQL : trouve le film dont l'identifiant est 2
    # requête préparée
    cursor.execute('''
                   SELECT
                    movie_id, title, duration, rate, release_year, m.director_id, d.fullname director_fullname
                   FROM movie m
                   INNER JOIN director d ON m.director_id = d.director_id
                   WHERE movie_id = %s
                   ''', (movie_id,))
    movie = cursor.fetchone()

    if movie:
        return jsonify(movie)
    else:
        return 'Film non trouvé', 404

# Ajouter un genre (grace à son identifiant) à un film (grace à son identifiant) :
# - POST  /:movie_id/category/:category_id
@movie_controller.route('/<int:movie_id>/category/<int:category_id>', methods=['POST'])
def add_category_to_movie(movie_id, category_id):
    cursor = mysql.connection.cursor()
    cursor.execute(
        'INSERT INTO movie_has_categories (movie_id, category_id) VALUES (%s, %s)',
        (movie_id, category_id)
    )
    # Applique les modifications
    mysql.connection.commit()
    return "La categorie %s a bien été ajoutée pour le film %s" % (category_id, movie_id), 201


# ajouter un film à la base de données
@movie_controller.route('', methods=['POST'])
def post_movie():
    # récupère le body de l'appel du endpoint POST
    movie = request.get_json()
    # insérer movie en base de données

    # transaction
    cursor = mysql.connection.cursor()
    cursor.execute(
        'INSERT INTO movie (title, release_year, duration, rate) VALUES (%s, %s, %s, %s)',
        [movie['title'], movie['release_year'], movie['duration'], movie['rate']]
    )

    # Applique les modifications
    mysql.connection.commit()

    # Récupère l'identifiant du dernier enregistrement inséré
    movie_id = cursor.lastrowid

    # Construit l'objet à retourner avec toutes les informations du film
    movie_inserted = {
        'movie_id': movie_id,
        'title': movie['title'],
        'release_year': movie['release_year'],
        'duration': movie['duration'],
        'rate': movie['rate']
    }

    # return "Un nouveau film a été inséré avec l'identifiant " + str(movie_id), 201

    # TODO améliorer le retour
    return movie_inserted, 201

@movie_controller.route('/title/<string:movie_title>', methods=['GET'])
def get_movies_by_title(movie_title):
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM movie WHERE title LIKE %s',
                   ('%' + movie_title + '%',))
    movieList = cursor.fetchall()

    return jsonify(movieList), 200

@movie_controller.route('/<int:movie_id>', methods=['DELETE'])
def delete_movie_by_id(movie_id):
    # TODO améliorer le système pour vérifier si l'élément existe avec de le supprimer
    cursor = mysql.connection.cursor()
    cursor.execute('DELETE FROM movie WHERE movie_id = %s', (movie_id,))
    mysql.connection.commit()
    return "Le film a été supprimé avec l'id " + str(movie_id), 200

# methode PUT de modification d'un film
@movie_controller.route('/<int:movie_id>', methods=['PUT'])
def update_movie_by_id(movie_id):
    # récupère les données du film que l'on veut mettre à jour
    data_to_update = request.get_json()

    # vérifier que le film avec movie_id existe
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM movie WHERE movie_id = %s', (movie_id,))
    # existing_movie contient : soit Null si le film n'existe pas, soit toutes les données du film
    existing_movie = cursor.fetchone()

    if existing_movie:
        # créer une fusion de l'objet en base de données + les données à modifier
        movie_updated = dict()
        movie_updated.update(existing_movie)
        movie_updated.update(data_to_update)

        # faire la requête qui modifie le film
        cursor.execute('UPDATE movie SET title=%s, release_year=%s, duration=%s, rate=%s',
                       (movie_updated['title'],
                        movie_updated['release_year'],
                        movie_updated['duration'],
                        movie_updated['rate']))

        mysql.connection.commit()

        return jsonify(movie_updated)
    else:
        return "Le film n'existe pas", 404
