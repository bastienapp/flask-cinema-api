from flask import Flask
from flask_mysqldb import MySQL
from dotenv import load_dotenv
from flask_bcrypt import Bcrypt
app = Flask(__name__)

bcrypt = Bcrypt(app)

load_dotenv()  # take environment variables from .env

app.config.from_object('config')

mysql = MySQL(app)

from controllers.movie_controller import movie_controller
app.register_blueprint(movie_controller, url_prefix='/movies')

from controllers.director_controller import director_controller
app.register_blueprint(director_controller, url_prefix="/directors")

from controllers.authentication_controller import auth_controller
app.register_blueprint(auth_controller, url_prefix="/auth")

if __name__ == '__main__':
    app.run(debug=True)
