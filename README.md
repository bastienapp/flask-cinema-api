# API Backend Cinema

Installer les dépendances à partir du fichier requiments.txt : `pip install -r requirements.txt`

Créer un fichier `.env` à partir de `.env.sample` et renseigner les valeurs attendues.

## Tâches réalisées

SQL :
- Créer un base de données et l'utiliser
- Créer un utilisateur avec les droits sur la base de données
- Créer un table : movie (movie_id, title, duration, release_year, rate)
- Insérer des données dans la table

CRUD : CREATE (POST), READ (GET), UPDATE (PUT), DELETE
- GET    /movies : affiche la liste des films
- GET    /movies/:id : affiche un film grace à son id
- POST   /movies : crée un film
- PUT    /movies/:id : met à jour un film grace à son id
- DELETE /movies/:id : supprime un film grace à son id