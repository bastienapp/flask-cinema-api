USE flask_cinema;

CREATE TABLE director (
	director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	fullname VARCHAR(255) NOT NULL
);

INSERT INTO director (fullname) VALUES ('Greta Gerwig'), ('Ridley Scott'), ('Vicky Jenson'), ('Michel Durand');

ALTER TABLE movie
ADD COLUMN director_id INT NOT NULL;

SELECT * FROM movie;
SELECT * FROM director;

UPDATE movie SET director_id = 1 WHERE director_id = 0;

ALTER TABLE movie
ADD FOREIGN KEY (director_id) REFERENCES director(director_id);

UPDATE movie SET director_id = 2 WHERE movie_id = 2;
UPDATE movie SET director_id = 3 WHERE movie_id = 3;
UPDATE movie SET director_id = 4 WHERE movie_id = 4;

-- requête qui permet de récupérer les infos d'une film et le nom du réalisateur⋅trice
-- > JOINTURE
SELECT movie_id, title, duration, m.director_id, d.fullname
FROM movie m
INNER JOIN director d ON m.director_id = d.director_id
WHERE movie_id = 2;

CREATE TABLE category (
	category_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	category_name VARCHAR(255) NOT NULL
);

CREATE TABLE movie_has_categories (
	movie_id INT NOT NULL, -- 2
	category_id INT NOT NULL, -- 1
	PRIMARY KEY (movie_id, category_id),
	FOREIGN KEY (movie_id) REFERENCES movie(movie_id),
	FOREIGN KEY (category_id) REFERENCES category(category_id)
);

INSERT INTO category (category_name) VALUES ('Horreur'), ('Animation'), ('Humour'), ('Science-Fiction'), ('Fantastique');


SELECT * FROM category;

-- Faire en sorte qu'Alien soit un film d'horreur
INSERT INTO movie_has_categories(movie_id, category_id) VALUES (2, 1);
-- Shrek (3) à les catégories Humour (3) et Animation (2)
INSERT INTO movie_has_categories(movie_id, category_id) VALUES (3, 3), (3, 2);

-- Pour le film qui a pour titre "Shrek", récupérer les noms de toutes ses catégories (category)
SELECT category_name
FROM movie m
INNER JOIN movie_has_categories mhc ON m.movie_id = mhc.movie_id
INNER JOIN category c ON mhc.category_id = c.category_id
WHERE m.title = "Shrek";

SELECT * FROM movie_has_categories;